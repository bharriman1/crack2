#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void decomment(char *s);

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Please supply two filenames, first to read and second to write\n");
        exit(2);
    }
    
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    FILE *fpout;
    fpout = fopen(argv[2], "w");
    if (!fpout)
    {
        printf("Can't edit %s for writing\n", argv[2]);
        exit(3);
    }
    
    char line[15];
    while(fgets(line, 15, fp) != NULL)
    {
        for(int i=0;i<strlen(line);i++)
        {
            if(line[i]=='\n')
            {
                line[i]='\0';
            }
        }
        char *hash = md5(line,  strlen(line));
        fprintf(fpout, "%s\n", hash);
    }
    
    fclose(fp);
    fclose(fpout);
}